import { Component, OnInit } from '@angular/core';
import { Artist } from '../model/artist';
import { GetArtistDbService } from '../_services/get-artist-db.service';

@Component({
  selector: 'app-music-details',
  templateUrl: './music-details.component.html',
  styleUrls: ['./music-details.component.css']
})
export class MusicDetailsComponent implements OnInit {

DisplayDetailsArtist: Artist = new Artist;

  constructor(private getArtistDbService : GetArtistDbService) { }

  ngOnInit(): void {
    this.DisplayDetailsArtist = this.getArtistDbService.selectedArtist;
  }

}
