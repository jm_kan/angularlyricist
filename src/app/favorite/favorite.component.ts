import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Artist } from '../model/artist';
import { GetArtistDbService } from '../_services/get-artist-db.service';
import { TokenStorageService } from '../_services/token-storage.service';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css']
})
export class FavoriteComponent implements OnInit {

  favorites : Artist [] = [];
  favoriteId: any[] = [];
  favorite : any ;
  username : any;
  


  constructor(private getArtistDb : GetArtistDbService, private tokenStorage : TokenStorageService) { }

  ngOnInit(): void {
    //changement de la ligne 25, rajout du this
    this.username = this.tokenStorage.getUser().username;
    this.getArtistDb.getAllFavorite(this.username).subscribe(result =>{
      for (let item of result){
        this.favoriteId.push(item.malId);
      }
      for ( let id of this.favoriteId){
        console.log( "id is" , id)
        this.getArtistDb.getArtistsByName(Number (id )).subscribe(result =>{
        if (result! == null ){
         }else {
          this.favorites.push(result );
          console.log("result22", result);
         }
             });
           }
      console.log ( " all favorite " ,this.favorites)
    });
    }



public removeUser(favorite){
  let response = this.getArtistDb.deleteArtistById(favorite);
  response.subscribe(result => this.favorites = result);
}
}

