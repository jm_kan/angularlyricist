import { Component, OnInit } from '@angular/core';



import { Inject, NgZone, PLATFORM_ID } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { isPlatformBrowser } from '@angular/common';

import { GetArtistDbService } from '../_services/get-artist-db.service';


@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements OnInit {


  listArtists = [];
  private chart:any;

  constructor(@Inject(PLATFORM_ID) private platformId, private zone: NgZone, private getArtistDbService: GetArtistDbService) { }


  ngOnInit(): void {
    this.getData();
  }

  
  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }


  getData() {
    this.getArtistDbService.getArtists().subscribe((Response) => {
      console.log(Response);
      this.listArtists = Response;
      this.ngAfterViewInit("rank");
      this.ngAfterViewInit("peak_position");
      // this.initchart("weeks_on_chart");
      // this.initchart("rank");
    })
  }


  ngAfterViewInit(statType:any) {

/* Chart code */
// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

/**
 * Chart design taken from Samsung health app
 */

this.chart = am4core.create("chartdiv"+statType, am4charts.XYChart);
this.chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

this.chart.paddingBottom = 30;

this.chart.data = [];

if(this.listArtists && this.listArtists.length>0){
  this.listArtists.forEach(artist =>{
    if(artist.weeks_on_chart> 280)
  this.chart.data.push(
    {artist: artist.artist, statType: artist[statType], href: artist.images[3]?artist.images[3]:"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw8REhMQERISEBEWFhEbFRASEg8WFxgRFBYWFxcVFhgYHSggGBolHhMVIjIiJSkrLi4uGCA2ODQsNygvLisBCgoKDg0OGhAQGysmICUvKy8tLS0vLS0yNy0rLy8tLS0tLSstLS0rLS0tLy8tLS0tLS0tLS0vLy0tLS0tLS0vLf/AABEIAOEA4QMBIgACEQEDEQH/xAAcAAEAAgIDAQAAAAAAAAAAAAAABgcEBQIDCAH/xABCEAACAQIDBQUFBQYCCwAAAAAAAQIDEQQSIQUGMUFRE2FxgZEHIqGxwTJCYpLRFCNSU3KCMzQWJEODorKzwtLh8f/EABoBAQADAQEBAAAAAAAAAAAAAAADBAUGAgH/xAAyEQACAQIDBQcDAwUAAAAAAAAAAQIDEQQhMQUSQVFhE3GBscHR8CKRoRTh8SMyM5Ky/9oADAMBAAIRAxEAPwC8QAAAAAAAAAAAAAAAAADrq1FFOUmlFJttuySWrbfJHYQz2iY29NYRSa7RXm4uzyJ6R8G1r3LvPdOm5yUUR1aqpQc2Qvevf2riajo4WUqeHvbMrqVT8TfFQ7unHouO628E8JUTV5U5WzwvxXVd6I5Tw0YPRa9Wc0dDDDwjDs2sjk6uMnOoqsXmtPbu6HoaEk0muDOZrdlYm6yPikmvqbI5u1jsEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACgN6tsVK2Kr1VOSi5yUEpzSyQ92Pqkn5l37cxHZYavUvZxpVGm+qi7fGx58rUPxw9ZfoWcNUhBtyaXeWKGGnVzjG6XiamWKqfzJ/nmbjDbu7WqR7SGHxTha93njddUpNN+RPfZdufTt+3V4xnq+wi1dKzs6rTWruml0s30tahYq41J/Rn19rFd0lTbg0rrXIofcjeDERx1BTq1JQnPJKMqlSS/eXjHRvlJxfkXwebMVLscXOS0VOtJr+2pdfI9JJ31K2JtdNcjV2rTUZxnFKzXD7+p9ABXMoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAj2/cmsBiLcXGK/NOKfwbKSjgqkpqEbXlJKPH7UnZfFl2b+r/Ua3+7/AOpErLdWmp47DJ/zU/yXn/2mbim3WUei/LsdHsie5hpy5Nv7RTLl2fhIUaVOjDSNOEYrwikvoc8ViI04SqSdoxjKTfdFXfyO8028uIUaLp6N1NLfh+95cvM0jnoxc5JczzzinOcpVGpaym27O15Nvj5npnC/Yj/TH5Fcunf3eulvEsqKtoS1Z71jS2niO2ccrWv6dFyOQAIjLAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANLvfRc8HiEtXkvb+hqX0Kv3MpSW0MM3FpZqurT/lTLixlDtKc6b+9GUfzK31Kg2fj1RxVCdR5VCr77fJcJN+F2ZuMk416b4ez/AHNzZbcqFamuT/MWvQuSc1FXeiRDto9tXqOfZztwirPSP6mnxu97xWKpUqV40Iyvro5yV7OXRdF5vumez5qWq6FqOIU5WjwKn6aphUpVFm1py/fyNBg9m1XVp5oSSzxu2nayd38icGPRjr4GQWL3RTrVHOQAAIgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAcW7asA669aMIynNqMYptyfBJcWURvNiYVK9SpTTjTnUlJJ8bSlfy5u3K5Nd8dv9u+xpP8AdRerX35Ln/SuXr0IHtOj9nxZjYnFwq1FCOi49beXmdTsbCuj9c9ZcOS69fT8ZG7f+Yp+L+TLQ2NUak/AqvY6tWh4v5MsDdrCyrVbXeVWcn3X4ef6leOJcKsacY3ctM7fOZLtOCacm7JL1J1hk7XfF/I7j5Y+nQnIgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAjW8tWVWPY055ItpTaV3L8K14devz2W19oKmlFfal8ER2WLzNuGtvg2Q17dnJMt4am7qfLQ1s92Yr/av8i/U0e3NjQgofvVq5cVFcvEkVdyfF38yO7x7DxWJpTq01+7oxu03ZysrycNPeyrV/C70MONCLlaCN7CVZOpHtKiS5u3HJcOL9+B07D2JOdaCp3k9Xw0t1b5ItnZOzoYemoR1fGUur/Qqv2P43Jip0ZP7dOdo/ii4yXwzlyGlg8NGLdRu8tO5fvx+xS27KpCv2Mnkkn3/wAO68AADQMMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGJtHFxo051ZcIq9ur5Jd7dkZZCvaDjHlhRjO3OUVG7k3pFdyWr9DzJ2RLQpdrUUOZgYnafaTcpNyaS0jzlLjboYWIrTScn7i5Rjy8+ppMJ2kXncnFOds2l02rtv0NzhNlVsXUVPtakbWc3xioX4rpJ8kV5x347q7+HhxN504QWbslr8+cDM3cwtfFT1bVCL96duL/gi+vfyJ+sNBQ7NJKFsuXllatY44LCU6MI06ayxirJfV9W+phbY29hsLHNWmot8IJpzf8ATH68O890aUaMfUxa9SWIqWgu5L5rxKX3Vbw+PpTbyuFaMWu6UnCXwcvQv887YvGqeIq4lQyxlUm8qd3Fym5Kz56kkre1LHxk7Qw7XL3KnD85BRqKDcX85nTbZwNbFTpzja9rO7Sz1t+WXKCA7u+0JV43qUkraSdO+j6pS4rzJngMfSrxzU5KS5rg0+jXInp4mlUk4RlmtVx+3HvWRy1bD1aLtNW+dDLABOQgAAAAAAAAAAAAAAAAAAAAAAAAAAAqbe7a8M9SouMpyjGT6QeX3V5N3LXm9H4M817Qxk52U+K5dL6/U+pLNvJLj7ddTa2LQVSc5Pgl+b+xJXVUqbirtvI4pcXLLbTq2y1t1tlvDUIQqPNWaTqy01nbhfmlw/8ApWvs5w37RiaSekaEc775wtGK/NLN/aXKR05NxzX8cD5tWra1Hxfovtn49CK787zrA0rQs8RUUuzT1UUuM5LotLLm/MpR1J16rnWnJuTvJt3k35mz352s8TjKs73jGThDuhBuKt46y/uI8pvloVazbfz7m7snBuhSW7lKSu336Lw4rn+MzHNRk4R0XC3hz8Td0tk0qtKnKScZZV70Wl3q/I7v9E6/ZRxU4tU2qbs9G3JeN7fqb3dzZLxbqQhKNN01HRpu9/Dglp6mbWjWluxpJ3WfLzsfcdjqSpRcZ6PN9bad/M1Wx9lxpO8ZSlfSzt9F3G8w2Iq0JqpB5ZL0fc1zR2Yjd/GUJr3Iyhr78WmuHO9mvQ+VsFVt9iyvf7UePqZlXD4ztVLck3zSb/KRkTrwq5yknfuLA2PtGGIpqpHR8JR/hlzRnkK3Rc6TamrKcrcV004d5NTrMO5ypRlUVm1nwMKqoxm1HQAAmPAAAAAAAAAAAAAAAAAAAAAAAABxlwPOe8WzewqqLad1dtcL8Hb0PRxS/tN2fKNSc7QjFTskneUozTlfuinpbxK+I3UlJvou9tfEbew6rVWVO/8Ad6X9zbexWkn+1VOd6MfK0n+hZ1R2TfRP5FX+xOt/moc32Ul/bmT/AOZFptElJ3hcqbXUv1c97XL/AJVjzNUoupJpatyfm73ZPvZhulCpmxdeOaMZONOElo5Rs3KS524Jdb9CMVsE6GJrU2rdm6i8lPKn6O5cG4zj+yRUeKnUzf1OWb5SRFOUZYjdtwv4/GjpduYlww/9LJStn0ab8rfcyd7YXwlZ2zZYZ7dezam16RZX24W1acdqTpQzZK0JKOa32opTs9ekWWpiaCqQlTlwlGUX4SVn8zzpu7i5YfakHJ/YrU1N66RzZJ/CUiayU958n5o5WklOjucd5WXfGXlb8npCcU1Zq66Fc75bbpbPrKFWFWUJxzU5QUGrJ2lHWS1WnqiyCuvbTgI1MJSqvjTrJX/BUi016qHoSwScrMgpXc0lxyM+lVutPLxJjQnmjGXVJ+qK6WJa0i3fxJ7si/YUr8ezhfxyoihPeyJK8FGzM0AHsrgAAAAAAAAAAAAAAAAAAAAAAAAhm/8AsqNSmqjdlbLK6vHjeLfR3ur+BMzHxeGhVhKnNXjJNNdzPFSCnFxZLRqulUU1wKe3QqLAYyE3JdjO9ObT4Rlls34NJ36XLpKU3q2bPC1HTlz1hPlKHJrv6rkSzcHehOnDD4iVpq8YVJNe9FcIyfXknz8eNTD1VCTpt5mztGjUxFNYpZ5Z9Vwdumj45aZNnR7Qd3JOf7VSWkssaite0kmlJ/ha0fel1MPc3a7w0lnkpUals7+9Cdk1J9VbTw8CzpxTTTSaa1T6PkQ3be4VOreVCo6MtPdavHTglbVLTvFehU3+0pPPl85keFx9KpR/T4l/Tonrlw6q2Vn0tprMoSTSad01o1wafMoffrDOntHEx4LNni786ijO/wDxMtndXZuKwsHSrThVgneDi5OUb8Yu8V7vTp4cIL7YNnz/AGilVhCUs1Ozyxk9YOXGy6Sj6ElX64JtW7+p92TNUcY4QkmmnZrjbPj3PIs/Y+LVahRq/wAdOnL80U38yJe2DEqOCjTfGpWiku6MZSfyXqZ/s3rSeApRmnGVNzg8ya0TzLjyyyXoV37SdrVMZilGnCo6FFSjB5J2nOVs81pqtEl4X5lqMrNNlXDYeUcXurSMs+5Ml2HV2kuLsku96E9oUlCMYrkkvREf3ewEXLtLaR4N85f+iSkGHj9N+ZFjKu9JRXAAAnKYAAAAAAAAAAAAAAAAAAAAAAAAAABqtu7Go4uk6VVd8Zq2aEv4ov6cypN7d1cThGnFOdGKsqsU2rttttfdfDjp3svA4yino1ddGfFGKnv2zNHAbSq4OX05rk/R8PmpTOyd+sThqUFftrZk6dVyfuqzTT4x0drarR6Ey2F7Q6GI92VGrSkld/YlFeL0fPoZ+1dxtn4i7dPsm9c1LLHXwacfgYeH3JjRjlpSj3uUbOT72j5KGV4Nrpqvcu1sTsyvC7g1Nvu8rprwT6myq74YKLUXUeZ8Eot/LwOjbG1KdTLlurXvKVkrSSdr9dF6kb2l7PMRVqdop0YO1tJVOPC/2Dv2ruvjEpVp1Kc4005RpRlU0jFctEnK19WU6qrSi00rfOpEqWBi4uFR343vr/ql43/fsxW3IQhkU7JvV62vbm/Iz9kbLdZRm04K6bk/vLj7vXxNPujXpVK0FNQqXzJKcb2la6krrjpa/eWTYhwuGVS8py3lovM8Yyp2D7OCs9WzroUowioxVkuR2gGoZIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOnEV4U4uc5KMUruT4JAHcCBbS9oajJxo0M6X36kst/CKT08WbDY2+UauVVqfYuX3ovMteF9E18Ss8ZRTs5L0++h9cWtSWnGSvoE76o5FrQ+EHq7NWEbnTgrRndW42zKyJtF31I9vFQqSzZXbRW8Xa5uNn37KnfjkhfxSVyjhKUKUpxgrZ+Raryc4Rk/mhlgAulUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAED362g51Fh0/chZyXWbV1fwTXqTwq/bt3iK1/5k/S+nwM3atRwopLi7P7N+gTzNQsPG+a2pl4dpSTfC5wsckjm5PeVmet5p3RPt1sW5RlSlrls4v8AC+Xk/mb8hW42Z1Kj+6oJebkrfJk1On2XKTwsN53tdeCbS/B9qSUpNo120XxfS30MjAu9OLfT6mDjKl4zfe/hZfQ2GEjaEF+GPrYvJ3PUv7Ed4APpEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACCb47OcKvbJe5Utd9JJWa80r+pOzpxNCFSLhNKUXxTK2Lw6r09zjqn1+ZPoCqrGVgNn1a0stOLk+b4W8XyN1tXdapC8qN6kP4fvL/AMjQvPCX3oTXimvqcxUoSoT/AK0Xbp6PP37j4WFsPZccNTyXzSes5dX3dyPu1NoxprLHWb5LkurIbs/aNeU1GVWo1Z6OUv1NpQw06jtGLffy82bVLGRnTUaMWuHyx9RtsDHtKaVrXevgnqbow9n4TsoKLd3rd+PJGYadNNQSep6cmwAD2eQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaDe3/DXmfQR1v8M+48y0Ivu//iLz+RYND7KAKWy/8PifInaADRPYAAAAAAAAAAAAAAAAAB//2Q=="}
    )
  console.log(artist);
})
}


let categoryAxis = this.chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "artist";
categoryAxis.renderer.grid.template.strokeOpacity = 0;
categoryAxis.renderer.minGridDistance = 10;
categoryAxis.renderer.labels.template.dy = 35;
categoryAxis.renderer.tooltip.dy = 35;

let valueAxis = this.chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.renderer.inside = true;
valueAxis.renderer.labels.template.fillOpacity = 0.3;
valueAxis.renderer.grid.template.strokeOpacity = 0;
valueAxis.min = 0;
valueAxis.cursorTooltipEnabled = false;
valueAxis.renderer.baseGrid.strokeOpacity = 0;

let series = this.chart.series.push(new am4charts.ColumnSeries);
series.dataFields.valueY = "statType";
series.dataFields.categoryX = "artist";
series.tooltipText = "{valueY.value}";
series.tooltip.pointerOrientation = "vertical";
series.tooltip.dy = - 6;
series.columnsContainer.zIndex = 100;

let columnTemplate = series.columns.template;
columnTemplate.width = am4core.percent(50);
columnTemplate.maxWidth = 66;
columnTemplate.column.cornerRadius(60, 60, 10, 10);
columnTemplate.strokeOpacity = 0;

series.heatRules.push({ target: columnTemplate, property: "fill", dataField: "valueY", min: am4core.color("#5faa46"), max: am4core.color("#e5dc36") });
series.mainContainer.mask = undefined;

let cursor = new am4charts.XYCursor();
this.chart.cursor = cursor;
cursor.lineX.disabled = true;
cursor.lineY.disabled = true;
cursor.behavior = "none";

let bullet = columnTemplate.createChild(am4charts.CircleBullet);
bullet.circle.radius = 30;
bullet.valign = "bottom";
bullet.align = "center";
bullet.isMeasured = true;
bullet.mouseEnabled = false;
bullet.verticalCenter = "bottom";
bullet.interactionsEnabled = false;

let hoverState = bullet.states.create("hover");
let outlineCircle = bullet.createChild(am4core.Circle);
outlineCircle.adapter.add("radius", function (radius, target) {
    let circleBullet = target.parent;
    return circleBullet.circle.pixelRadius + 10;
})

let image = bullet.createChild(am4core.Image);
image.width = 60;
image.height = 60;
image.horizontalCenter = "middle";
image.verticalCenter = "middle";
image.propertyFields.href = "href";

image.adapter.add("mask",(mask, target) =>{
    let circleBullet = target.parent;
    return circleBullet.circle;
})

let previousBullet;
this.chart.cursor.events.on("cursorpositionchanged",  (event)=> {
    let dataItem = series.tooltipDataItem;

    if (dataItem.column) {
        let bullet = dataItem.column.children.getIndex(1);

        if (previousBullet && previousBullet != bullet) {
            previousBullet.isHover = false;
        }

        if (previousBullet != bullet) {

            let hs = bullet.states.getKey("hover");
            hs.properties.dy = -bullet.parent.pixelHeight + 30;
            bullet.isHover = true;

            previousBullet = bullet;
        }
    }
})



  }



  ngOnDestroy() {
    // Clean up chart when the component is removed
    this.browserOnly(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });

}

    }