import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Artist } from '../model/artist';
import { GetArtistDbService } from '../_services/get-artist-db.service';
import { TokenStorageService } from '../_services/token-storage.service';
import { UserService } from '../_services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  username:String = new String ;
  content: string;
  artists: Artist[] = [];
  DisplayDetailsArtist: Artist = new Artist;
  form : any={};

  constructor(private userService: UserService, private getArtistDbService: GetArtistDbService, private router: Router, private tokenStorageService: TokenStorageService) { }
  isLoggedIn = false;
  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken();
    this.userService.getPublicContent().subscribe(
      data => {
        this.content = data;
      },
      err => {
        this.content = JSON.parse(err.error).message;
      });

    this.getArtistDbService.getArtists().subscribe(result => {
      console.log("result is", result)
      for (let item of result) {
        this.artists.push(item);
      }
      console.log(this.artists);
    });
  }

  onSearch() :void {
    this.artists=[]
    this.getArtistDbService.getArtistsByName(this.form.search).subscribe(result =>{
      console.log("notre artist by Name ",result)
      for (let item of result ) {
        this.artists.push(item);
      }
    })
  }


  onSelectFavorite(artist){
    this.getArtistDbService.addFavoriteArtist(this.username).subscribe(result=>{
        console.log("favorite added");
        this.router.navigate(['/favorite']);
    })
  }

  onSelectArtist(artist){
    this.getArtistDbService.DisplaySelectedArtist(artist);
    this.router.navigate(['/music-details']);
  }
}
