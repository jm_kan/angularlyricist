import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { Artist } from '../model/artist';

@Injectable({
  providedIn: 'root'
})

export class GetArtistDbService {

  selectedArtist: Artist;

  constructor(private http:HttpClient) { }

  getArtists():Observable<any>{
    return this.http.get<any>(environment.apiNode + `/artistprojet/artists`);
  }

  getArtistsByName(artist):Observable<any>{
    return this.http.get (environment.apiNode + `/api/artistsByStartDate/findByName/${artist}`);
  }

  DisplaySelectedArtist(selectedArtist :Artist){
    this.selectedArtist = selectedArtist ;
  }

  addFavoriteArtist(username: String) {
    return this.http.post(environment.apiSpring+ `/user/favoriteartist/add/${username}`,null );
    }

  getAllFavorite(username : string) : Observable<any[]> {
      return this.http.get<any[]>(environment.apiSpring+ `/user/favoriteartist/all/${username}`)
    }
    
  deleteArtistById( username : string) : Observable<any[]>{
    return this.http.get<any[]>(environment.apiSpring+ `/user/favoriteartist/delete/{username}/{mal_id}`)
  }

  }
