import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Artist } from '../model/artist';

@Injectable({
  providedIn: 'root'
})
export class GetDataService {

  constructor(private http:HttpClient) { }

  getArtists(): Observable<Artist[]>{
    return this.http.get<Artist[]>(environment.apiNode + "/artistprojet/artists");
  }



}
