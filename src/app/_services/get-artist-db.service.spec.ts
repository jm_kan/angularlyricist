import { TestBed } from '@angular/core/testing';

import { GetArtistDbService } from './get-artist-db.service';

describe('GetArtistDbService', () => {
  let service: GetArtistDbService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GetArtistDbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
